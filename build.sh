#!/bin/bash
PATH="/usr/local/go/bin:$PATH"
export GOPATH=$(pwd):~/devLibs/gopath

rm -rf bin/*

export GOARCH=386
export GOOS=windows
go install flex6k-dax-audio-grabber

export GOARCH=amd64
export GOOS=windows
go install flex6k-dax-audio-grabber

export GOARCH=amd64
export GOOS=linux
go install flex6k-dax-audio-grabber

export GOARCH=amd64
export GOOS=freebsd
go install flex6k-dax-audio-grabber

export GOARCH=386
export GOOS=linux
go install flex6k-dax-audio-grabber

export GOARCH=arm
export GOOS=linux
export GOARM=5
go install flex6k-dax-audio-grabber


