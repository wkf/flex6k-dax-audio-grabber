Simple and fast tool to grap flex6k radio DAX audo channel data.
Output is raw: Float 32 bit - Big Endian, Rate 48000 Hz, Mono

No error handling, use on your own risk. The purpose is simply to get the VITA IF data part out to Stdout

==Usage examples==

record: 
./flex6k-dax-audio-grabber --RADIO=192.168.92.8 --ME=192.168.178.71:7789 --CH=1 > myrecorod.dat
aplay -c 1 -t raw -f FLOAT_BE -r 48000 myrecorod.dat 

play direct: 
./flex6k-dax-audio-grabber --RADIO=192.168.92.8 --ME=192.168.178.71:7789 --CH=1 | aplay -c 1 -t raw -f FLOAT_BE -r 48000 myrecorod.dat -

ffplay:
bin/flex6k-dax-audio-grabber --RADIO=192.168.92.8 --ME=192.168.178.71:7789 --CH=1 | ffplay -f f32be -ar 48k -ac 1 -


