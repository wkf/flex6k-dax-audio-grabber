package main

import (
	"log"
	"net"
	"time"
	"encoding/binary"
	"os"
	"flag"
	"strings"
)

type vita_header struct {
	pkt_type     uint
	c            bool
	t            bool
	tsi          uint16
	tsf          uint16
	packet_count uint16
	packet_size  uint16
}

type vita_if_data struct {
	header         *vita_header
	stream_id      uint32
	class_id_h     uint32
	class_id_l     uint32
	timestamp_int  uint32
	timestamp_frac uint64
	payload        [] float32
}

func main() {
	var radioAddr string
	var myAddressAndPort string
	var daxChan string

	flag.StringVar(&radioAddr, "RADIO", "", "IP ADDRESS OF THE RADIO e.g 192.168.41.8")
	flag.StringVar(&myAddressAndPort, "ME", "", "LOCAL IP ADDRESS and UDP Port to use e.g. 192.168.45.155:7788")
	flag.StringVar(&daxChan, "CH", "", "DAX AUDIO CHANNEL NUMBER e.g. ")
	flag.Parse()

	 meIp := strings.Split(myAddressAndPort,":")[0]
	 mePort := strings.Split(myAddressAndPort,":")[1]

	tcpAddr, err := net.ResolveTCPAddr("tcp", radioAddr+":4992")

	if err != nil {
		log.Fatal(err)
		panic(err)
	}

	conn, err := net.DialTCP("tcp", nil, tcpAddr)

	if err != nil {
		log.Fatal(err)
		panic(err)
	}

	if err != nil {
		log.Println(err)
		panic(err)
	}

	defer conn.Close()
	go printOutput(conn)

	_, err = conn.Write([]byte("C1|stream create dax="+daxChan+"ip="+meIp+" port="+mePort+"\r"))

	go ServerListener(myAddressAndPort)

	for {
		i := 1;
		i++
		time.Sleep(time.Second * 10)
	}
}

func printOutput(conn *net.TCPConn) {

	l := log.New(os.Stderr, "RADIO_MSG", 0)
	buf := make([]byte, 2048)

	for {
		n, err := conn.Read(buf)

		if (err != nil) {
			continue
		}

		l.Println(string(buf[:n]))

		if err != nil {
			l.Println(err)
		}
	}
}

func ServerListener(listenAddr string) {

	FLexBroadcastAddr, err := net.ResolveUDPAddr("udp", listenAddr)

	if (err != nil) {
		panic(err)
	}

	ServerConn, err := net.ListenUDP("udp", FLexBroadcastAddr)

	if (err != nil) {
		panic(err)
	}

	defer ServerConn.Close()
	buf := make([]byte, 4096)

	if err != nil {
		panic(err)
	}

	if err != nil {
		panic(err)
	}

	for {
		ServerConn.ReadFromUDP(buf)

		var header vita_header
		var ifdata vita_if_data

		ifdata.header = &header;
		idx := 0;
		payload_bytes := 0;
		rHeader := binary.BigEndian.Uint32(buf[idx:4]) // header

		header.pkt_type = uint((rHeader >> 28))
		header.c = ((rHeader & 0x08000000) != 0);
		header.t = ((rHeader & 0x04000000) != 0);
		header.tsi = uint16(((rHeader >> 22) & 0x03))
		header.tsf = uint16(((rHeader >> 20) & 0x03))
		header.packet_count = uint16(((rHeader >> 16) & 0x0F))
		header.packet_size = uint16(rHeader & 0xFFFF)
		idx += 4

		if (header.pkt_type == 1 || header.pkt_type == 3) {
			ifdata.stream_id = binary.BigEndian.Uint32(buf[idx:idx+4])
			idx += 4;
			payload_bytes -= 4
		}

		if (header.c) {
			idx += 8;
			payload_bytes -= 8
		}

		if (header.tsi != 0) {
			idx += 4;
			payload_bytes -= 4
		}

		if (header.tsf != 0) {
			idx += 8;
			payload_bytes -= 8
		}

		if (header.t) {
			payload_bytes -= 4
		}

		payload_bytes += int((header.packet_size - 1) * 4);
		os.Stdout.Write(buf[idx:payload_bytes])
	}
}